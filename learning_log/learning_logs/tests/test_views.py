from django.test import TestCase, Client
from django.urls import reverse
from learning_logs.models import Topic, Entry
from django.contrib.auth import get_user_model

User = get_user_model()

class PostsPagesTests(TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()
        cls.user = User.objects.create_user(username='author')
        cls.topic = Topic.objects.create(
            text='Test topic',
            owner=cls.user
        )

        cls.entry = Entry.objects.create(
            text='Test entry',
            topic_id=cls.topic.id
        )

    def setUp(self) -> None:
        self.guest_client = Client()
        self.user = User.objects.create_user(username='not_author')
        self.not_author_client = Client()
        self.not_author_client.force_login(self.user)
        self.author_client = Client()
        self.author_client.force_login(PostsPagesTests.user)

    def test_urls_path_correct_template(self):
        reverse_names_templates = {
            reverse(
                'learning_logs:index'): 'learning_logs/index.html',
            reverse(
                'learning_logs:topics'): 'learning_logs/topics.html',
            reverse(
                'learning_logs:new_topic'): 'learning_logs/topics.html',
            reverse(
                'learning_logs:topic',
                kwargs={'topic_id': f'{PostsPagesTests.topic.id}'},
            ): 'learning_logs/topic.html',
            reverse(
                'learning_logs:new_entry',
                kwargs={'topic_id': f'{PostsPagesTests.topic.id}'},
            ): 'learning_logs/new_entry.html',
            reverse(
                'learning_logs:edit_entry',
                kwargs={'entry_id': f'{PostsPagesTests.entry.id}'},
            ): 'learning_logs/edit_entry.html',
            reverse(
                'learning_logs:remove_topic',
                kwargs={'topic_id': f'{PostsPagesTests.topic.id}'},
            ): 'learning_logs/topics.html',
            reverse(
                'learning_logs:remove_entry',
                kwargs={'entry_id': f'{PostsPagesTests.entry.id}',
                        'topic_id': f'{PostsPagesTests.topic.id}'}
            ): 'learning_logs/topics.html'
        }
        for reverse_name, template in reverse_names_templates.items():
            if reverse_name == '/remove_entry/1/1': reverse_name += '/'
            with self.subTest(reverse_name=reverse_name):
                response = self.author_client.get(reverse_name)
                try:
                    self.assertTemplateUsed(response, template)
                except AssertionError:
                    info = response.request['PATH_INFO']
                    print(f'{info} redirects to another page.')
    
    def test_index_content(self):
        response = self.not_author_client.get(
            reverse('learning_logs:index')
        )
        
        html = response.content.decode('utf8')
        self.assertIn('<h1 class="display-3">Track your learning.</h1>', html)
        
    def test_topics_content(self):
        response = self.author_client.get(
            reverse('learning_logs:topics')
        )
        
        html = response.content.decode('utf8')
        self.assertIn('<h1>Topics</h1>', html)
        self.assertIn('>Add a new topic</a>', html)
        
    def test_topic_content(self):
        response = self.author_client.get(
            reverse('learning_logs:topic', kwargs={'topic_id': f'{PostsPagesTests.topic.id}'}
            )
        )
        
        html = response.content.decode('utf8')
        self.assertIn('>Remove topic</a></small>', html)
        self.assertIn('>Add new entry</a></small>', html)

    def test_new_topic_content(self):
        response = self.author_client.get(
            reverse('learning_logs:new_topic')
        )
        
        html = response.content.decode('utf8')
        self.assertIn('<p>Add a new topic:</p>', html)
        self.assertIn('<button name="submit" class="btn btn-primary">add topic</button>', html)

    def test_new_entry_content(self):
        response = self.author_client.get(
            reverse('learning_logs:new_entry', kwargs={'topic_id': f'{PostsPagesTests.topic.id}'}
            )
        )
        
        html = response.content.decode('utf8')
        self.assertIn('<button name="submit" class="btn btn-primary">add entry</button>', html)
        self.assertIn('Add a new entry in:', html)
    
    def test_edit_entry_content(self):
        response = self.author_client.get(
            reverse('learning_logs:edit_entry', kwargs={'entry_id': f'{PostsPagesTests.entry.id}'}
            )
        )
        
        html = response.content.decode('utf8')
        self.assertIn('<p>Edit entry:</p>', html)
        self.assertIn('<button name="submit" class="btn btn-primary">save changes</button>', html)