from django.test import TestCase
from learning_logs.models import Topic, Entry
from django.contrib.auth import get_user_model 


class ModelsTest(TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.user = get_user_model()
        cls.author = cls.user.objects.create_user(username='authore')
        cls.topic = Topic.objects.create(
            text='Test topic',
            owner=cls.author
        )

        cls.entry = Entry.objects.create(
            text='Test entry',
            topic_id=cls.topic.id
        )


    def test_have_correct_print_name(self):
        models_expected = {
            f'{str(ModelsTest.topic)}': f'{ModelsTest.topic.text}',
            f'{str(ModelsTest.entry)}': f'{ModelsTest.entry.text}',
        }
        for model, expected in models_expected.items():
            with self.subTest(model=model):
                self.assertEqual(expected, model)


    def test_verbose_name_plural_entry(self):
        entry = ModelsTest.entry
        verbose_name_plural = entry._meta.verbose_name_plural
        self.assertEquals(verbose_name_plural, "entries")