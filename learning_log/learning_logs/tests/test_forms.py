from django.test import Client, TestCase
from django.urls import reverse
from django.contrib.auth import get_user_model
from learning_logs.models import Topic, Entry

User = get_user_model()


def check_fields_topic(self_name, form) -> None:
    form_data = form
    self_name.assertTrue(
        Topic.objects.filter(
            text=form_data['text']
        ).exists()
    )


def check_fields_entry(self_name, form) -> None:
    form_data = form
    self_name.assertTrue(
        Entry.objects.filter(
            text=form_data['text']
        ).exists()
    )


class PostCreateFormTests(TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()
        cls.user = User.objects.create_user(username='author')
        cls.topic = Topic.objects.create(
            text='Test topic',
            owner=cls.user
        )
        
        cls.entry = Entry.objects.create(
            text='Test entry',
            topic_id=cls.topic.id
        )

    def setUp(self) -> None:
        self.guest_client = Client()
        self.author_client = Client()
        self.author_client.force_login(PostCreateFormTests.user)
        self.user = User.objects.create_user(username='not_author')
        self.not_author_client = Client()
        self.not_author_client.force_login(self.user)
        self.topic = Topic.objects.create(
            text='Тестовый topic',
            owner=PostCreateFormTests.user
        )
        self.entry = Entry.objects.create(
            text='Тестовый entry',
            topic_id=PostCreateFormTests.topic.id
        )

    def test_post_create_author_new_topic(self):
        topic_count = Topic.objects.count()
        form_data = {
            'text': 'Test text create topic',
        }
        response = self.author_client.post(
            reverse(
                'learning_logs:new_topic'
            ),
            data=form_data,
            follow=True
        )
        check_fields_topic(self, form_data)
        self.assertRedirects(
            response,
            reverse('learning_logs:topics'
            )
        )
        self.assertEqual(Topic.objects.count(), topic_count + 1)

    def test_post_create_author_new_entry(self):
        entry_count = Entry.objects.count()
        form_data = {
            'text': 'Test text create entry',
        }
        response = self.author_client.post(
            reverse(
                'learning_logs:new_entry',
                kwargs={'topic_id': f'{self.topic.id}'}
            ),
            data=form_data,
            follow=True
        )
        check_fields_entry(self, form_data)
        self.assertRedirects(
            response,
            reverse('learning_logs:topic',
                    kwargs={'topic_id': f'{self.topic.id}'}
            )
        )
        self.assertEqual(Entry.objects.count(), entry_count + 1)

    def test_post_edit_entry_author(self):
        entry_count = Entry.objects.count()
        form_data = {
            'text': 'Test edit entry text',
        }
        response = self.author_client.post(
            reverse(
                'learning_logs:edit_entry',
                kwargs={'entry_id': f'{self.entry.id}'},
            ),
            data=form_data,
            follow=True
        )
        
        check_fields_entry(self, form_data)
        self.assertRedirects(
            response,
            reverse(
                'learning_logs:topic',
                kwargs={'topic_id': f'{self.entry.topic.id}'},
            )
        )
        self.assertEqual(Entry.objects.count(), entry_count)