from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied
from django.contrib.auth import logout
from guest_user.decorators import allow_guest_user
from guest_user.functions import is_guest_user
from .forms import TopicForm, EntryForm
from .models import Topic, Entry


def index(request):
    return render(request, 'learning_logs/index.html')


@allow_guest_user
def topics(request):
    topics = Topic.objects.filter(owner=request.user).order_by('date_added')

    context = {'topics': topics}
    return render(request, 'learning_logs/topics.html', context)


@login_required
def topic(request, topic_id):
    topic = Topic.objects.get(id=topic_id)
    entries = topic.entry_set.order_by('-date_added')

    check_topic_owner(topic.owner, request.user)

    context = {'topic': topic, 'entries': entries}
    return render(request, 'learning_logs/topic.html', context)


@login_required
def new_topic(request):
    if not request.POST:
        form = TopicForm()
    else:
        form = TopicForm(data=request.POST)
        if form.is_valid():
            new_topic = form.save(commit=False)
            new_topic.owner = request.user
            new_topic.save()

            return redirect('learning_logs:topics')

    context = {'form': form}
    return render(request, 'learning_logs/new_topic.html', context)


@login_required
def new_entry(request, topic_id):
    if is_guest_user(request.user):
        logout(request)
        return redirect('users:login')

    topic = Topic.objects.get(id=topic_id)

    check_topic_owner(topic.owner, request.user)

    if not request.POST:
        form = EntryForm()
    else:
        form = EntryForm(data=request.POST)
        if form.is_valid():
            new_entry = form.save(commit=False)
            new_entry.topic = topic
            new_entry.save()

            return redirect('learning_logs:topic', topic_id=topic_id)

    context = {'topic': topic, 'form': form}
    return render(request, 'learning_logs/new_entry.html', context)


@login_required
def edit_entry(request, entry_id):
    if is_guest_user(request.user):
        logout(request)
        return redirect('users:login')

    entry = Entry.objects.get(id=entry_id)
    topic = entry.topic

    check_topic_owner(topic.owner, request.user)

    if not request.POST:
        form = EntryForm(instance=entry)
    else:
        form = EntryForm(instance=entry, data=request.POST)
        if form.is_valid():
            form.save()
            return redirect('learning_logs:topic', topic_id=topic.id)

    context = {'entry': entry, 'topic': topic, 'form': form}
    return render(request, 'learning_logs/edit_entry.html', context)


@login_required
def remove_topic(request, topic_id):
    if is_guest_user(request.user):
        logout(request)
        return redirect('users:login')

    topic = Topic.objects.get(id=topic_id)

    check_topic_owner(topic.owner, request.user)

    Topic.objects.filter(id=topic_id).delete()

    return redirect('learning_logs:topics')


@login_required
def remove_entry(request, entry_id, topic_id):
    if is_guest_user(request.user):
        logout(request)
        return redirect('users:login')

    topic = Topic.objects.get(id=topic_id)

    check_topic_owner(topic.owner, request.user)

    Entry.objects.filter(id=entry_id).delete()

    return redirect('learning_logs:topic', topic.id)


def check_topic_owner(owner, user):
    if owner != user:
        raise PermissionDenied
