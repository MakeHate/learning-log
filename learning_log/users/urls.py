"""Определяет схемы URL для пользователей."""
from django.urls import path, include, reverse_lazy
from django.contrib.auth import views as auth_view
from . import views

app_name = 'users'
urlpatterns = [
     path('register/', views.register, name='register'),
     path('login/', auth_view.LoginView.as_view(
          redirect_authenticated_user=True),
          name='login'
          ),
     path(
          'password_change/',
          auth_view.PasswordChangeView.as_view(
               success_url=reverse_lazy(
                    'users:password_change_done'
               ), template_name='password/password_change_form.html'
          ), name='password_change'
     ),
     path('profile/', views.profile, name='profile'),
     path('', include('django.contrib.auth.urls')),
     path(
          'password_change_done/',
          auth_view.PasswordChangeDoneView.as_view(
               template_name='password/password_change_done.html'
          ), name='password_change_done'
     ),
     path(
          'reset_password/',
          auth_view.PasswordResetView.as_view(
               template_name='password/password_reset_form.html',
               email_template_name='password/password_reset_email.html',
               success_url=reverse_lazy(
                    'users:password_reset_done'
               )
          ), name='reset_password'
     ),
     path(
          'reset_password_sent/',
          auth_view.PasswordResetDoneView.as_view(
               template_name='password/password_reset_done.html'
          ), name='password_reset_done'
     ),
     path(
          'reset/<uidb64>/<token>',
          auth_view.PasswordResetConfirmView.as_view(
               success_url=reverse_lazy(
                    'users:password_reset_complete'
               ), template_name='password/password_reset_confirm.html'
          ), name='password_reset_confirm'
     ),
     path(
          'reset_password_complete/',
          auth_view.PasswordResetCompleteView.as_view(
               template_name='password/password_reset_complete.html'
          ), name='password_reset_complete'
     )
]
