from django.test import TestCase, Client
from django.contrib.auth import get_user_model
from django.utils.encoding import force_bytes
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from http import HTTPStatus
from base64 import urlsafe_b64encode

User = get_user_model()
account_activation_token = PasswordResetTokenGenerator()

class UsersURLTest(TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        """Устанавливаем fixtures"""
        super().setUpClass()
        cls.user, cls.created = User.objects.get_or_create(username='authore')
        cls.authorized_client = Client()
        cls.authorized_client.force_login(cls.user)

    def setUp(self) -> None:
        self.guest_client = Client()
        self.author_client = Client()
        self.author_client.force_login(UsersURLTest.user)
        self.user = User.objects.create_user(username='not_author')
        self.authorized_client = Client()
        self.authorized_client.force_login(self.user)

    def test_client_posts_urls_status_client(self):
        """Тестирование: доступность всех URLs."""
        user = UsersURLTest.user
        uid = urlsafe_b64encode(force_bytes(user.id)),
        token = account_activation_token.make_token(user)
        
        urls_status_codes = {
            '/users/register/': HTTPStatus.OK,
            '/users/login/': HTTPStatus.OK,
            '/users/reset_password/': HTTPStatus.OK,
            f'/users/reset/{uid}/{token}/': HTTPStatus.OK,
            '/users/reset_password_complete/': HTTPStatus.OK,
            '/users/password_change/': HTTPStatus.FOUND,
            '/users/password_change_done/': HTTPStatus.FOUND,
            '/users/profile/': HTTPStatus.FOUND
        }
        for url, status_code in urls_status_codes.items():
            with self.subTest(url=url):
                response = self.guest_client.get(url)
                self.assertEqual(
                    response.status_code,
                    status_code,
                    'Status code is fail!'
                )

    def tests_urls_correct_templates(self):
        """Тестирование: соответствие URL используемому шаблону."""
        user = UsersURLTest.user
        uid = urlsafe_b64encode(force_bytes(user.id)),
        token = account_activation_token.make_token(user)
        
        urls_templates_name = {
            '/users/register/': 'registration/register.html',
            '/users/login/':'registration/login.html',
            '/users/profile/': 'registration/profile.html',
            '/users/password_change/': 'password/password_change_form.html',
            '/users/password_change_done/': 'password/password_change_done.html',
            '/users/reset_password/': 'password/reset_password_form.html',
            '/users/reset_password_sent/': 'password/reset_password_done.html',
           f'/users/reset/{uid}/{token}/': 'paswword/password_reset_confirm.html',
            '/users/reset_password_complete/': 'paswword/password_reset_complete.html'
        }
        for url, template in urls_templates_name.items():
            with self.subTest(url=url):
                try:
                    response = self.author_client.get(url)
                    self.assertTemplateUsed(response, template)
                except AssertionError:
                    info = response.request['PATH_INFO']
                    print(f'{info} redirects to another page.')
                    pass