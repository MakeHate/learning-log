from django.test import TestCase
from django.contrib.auth import get_user_model
from django.core.files.uploadedfile import SimpleUploadedFile
from users.models import Profile
from users.forms import ProfileUpdateForm
from base64 import b64decode

User = get_user_model()

class ModelsTest(TestCase):
    @classmethod
    def setUpClass(cls):
        """Входные данные теста."""
        super().setUpClass()
        cls.author, cls.created = User.objects.get_or_create(username='authore')
        cls.profile = Profile.objects.create(id=cls.author.id,
                                             user=cls.author,
                                             image='default.jpg')

    def test_have_correct_print_name(self):
        """Тестирование: корректно ли работает __str__."""
        models_expected = {
            f'{str(ModelsTest.profile)}': f'{ModelsTest.author.username} Profile'
        }
        for model, expected in models_expected.items():
            with self.subTest(model=model):
                self.assertEqual(expected, model)

    def test_change_avatar_in_profile(self):
        """Тестирование: корректно ли работает смена аватарки в профиле."""
        profile = Profile.objects.get(id=ModelsTest.author.id,
                                      image='default.jpg')
        profileform = ProfileUpdateForm(instance=profile, 
                                        files={'image': SimpleUploadedFile(name='test_image.jpg', 
                                                                           content=b64decode("iVBORw0KGgoAAAANSUhEUgAAAAUA" + 
                                                                                             "AAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO" + 
                                                                                             "9TXL0Y4OHwAAAABJRU5ErkJggg=="), 
                                                                           content_type='image/jpeg')})

        if profileform.is_valid():
            profile = profileform.save()
        
        new_profile = Profile.objects.get(id=ModelsTest.author.id)
        self.assertNotEquals(profile.image, "default.jpg")
        self.assertEquals(new_profile.image, 'profile_pics/test_image.jpg')