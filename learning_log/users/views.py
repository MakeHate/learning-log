from django.shortcuts import render, redirect
from django.contrib.auth import login, logout
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from .forms import UserCreateForm, UserUpdateForm, ProfileUpdateForm
from guest_user.functions import is_guest_user


def register(request):
    user_auth = request.user.is_authenticated

    if not user_auth:
        if not request.POST:
            form = UserCreateForm()
        else:
            form = UserCreateForm(data=request.POST)

            if form.is_valid():
                new_user = form.save()
                login(request, new_user)
                return redirect('learning_logs:index')
    else:
        return redirect('learning_logs:index')

    context = {'form': form}
    return render(request, 'registration/register.html', context)


@login_required
def profile(request):
    if is_guest_user(request.user):
        logout(request)
        return redirect('users:login')

    if not request.POST:
        u_form = UserUpdateForm(instance=request.user)
        p_form = ProfileUpdateForm(instance=request.user.profile)

    else:
        u_form = UserUpdateForm(request.POST, instance=request.user)
        p_form = ProfileUpdateForm(request.POST,
                                   request.FILES,
                                   instance=request.user.profile)

        if u_form.is_valid() and p_form.is_valid():
            u_form.save()
            p_form.save()
            messages.success(request, 'Ваш профиль успешно обновлен.')

            return redirect('users:profile')

    context = {'u_form': u_form, 'p_form': p_form}
    return render(request, 'registration/profile.html', context)
