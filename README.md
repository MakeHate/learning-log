# Learning Log
**A blog for your personal use.**

# Information
_The blog was developed based on a project from Eric Metiz's book, but with a number of my additions and improvements. In particular, a guest user profile was added, user profiles, the ability to reset the password via mail or change it, a cool ckeditor post editor and much more, including Unit Testing._

## Clone this repository and fix .env.

```
git clone https://gitlab.com/MakeHate/learning-log.git
cd learning log
nano .env
```

## Setup

```
sudo docker-compose run learning_log django-admin startproject learning_log .
sudo docker-compose build
```

## Make db migrations
```
sudo docker-compose run learning_log python manage.py migrate
sudo docker-compose run learning_log python manage.py makemigrations learning_logs
sudo docker-compose run learning_log python manage.py migrate
```

## Create superuser
```
sudo docker-compose run learning_log python manage.py createsuperuser
```

## Up container and open suit in your browser
```
sudo docker-compose up
open http://0.0.0.0:3003/
```

# Deploy
- If desired, the project can be placed on a VPS and used, this will require a small configuration of nginx and gunicorn in the container.

# Offtop
- This project can be expanded and improved indefinitely, for example, by redesigning it for a clean backend, completely removing frontend and shifting towards REST. It's up to you.

# Screenshot
![insert_image2](https://i.ibb.co/QDxFQRD/2023-01-27-00-50-06.png)
